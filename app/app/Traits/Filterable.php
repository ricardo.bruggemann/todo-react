<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Filters\QueryFilter;

trait Filterable
{
    /**
     * Filter a result set.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}
