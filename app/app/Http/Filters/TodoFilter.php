<?php

namespace App\Http\Filters;

class TodoFilter extends QueryFilter
{
    /**
     * Filter the query by specific code
     *
     * @param  string $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function status($value)
    {
        return $this->builder->where('status', $value);
    }
}
