<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;
use App\Http\Filters\TodoFilter;
use App\Http\Requests\TodoRequest;
use App\Http\Requests\StatusRequest;
use Illuminate\Support\Facades\Log;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TodoFilter $filters)
    {
          $items = Todo::filter($filters)
                       ->paginate(50);

          return $this->returnPaginate($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Todo $todo, TodoRequest $request)
    {
        $todo->create($request->all());

        return $this->returnSuccess();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Todo $todo, TodoRequest $request)
    {
        $todo->update($request->all());

        return $this->returnSuccess();
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function status(StatusRequest $request)
    {
        // Find records and update
        Todo::where("todo_id", $request->todo_id)->update(["status" => $request->status]);

        if($request->status==1)
        {
            return $this->returnStatusActivate();
        }
        else
        {
            return $this->returnStatusDesactivate();
        }
    }
}
