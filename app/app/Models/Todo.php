<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\AuditOperations;
use App\Traits\Filterable;

class Todo extends Model
{
    use HasFactory, AuditOperations, Filterable;
    protected $table = 'todo';
    protected $primaryKey = 'todo_id';
    protected $fillable = ['description', 'status'];
}
