<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/todo', 'App\Http\Controllers\TodoController@index')->name('todo.index');
Route::put('/todo/store', 'App\Http\Controllers\TodoController@store')->name('todo.store');
Route::patch('/todo/update/{todo}', 'App\Http\Controllers\TodoController@update')->name('todo.update');
Route::patch('/todo/status/{todo}', 'App\Http\Controllers\TodoController@status')->name('todo.status');
