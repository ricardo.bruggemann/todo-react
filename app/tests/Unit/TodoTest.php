<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Todo;
use DB;

class TodoTest extends TestCase
{
    /** @test */
    public function user_can_get_data()
    {
        // Generate the data
        $attributes = factory('App\Models\Todo')->raw();

        // Send
        $response = $this->get(route('todo.index'));

        // Assert
        $response->assertStatus(200)->assertSee("data");
    }

    /** @test */
    public function user_can_store()
    {
        // Generate the data
        $attributes = factory('App\Models\Todo')->raw();

        // Send
        $response = $this->put(route('todo.store'), $attributes);

        // Assert
        $response->assertStatus(201)->assertSee("success");
    }

    /** @test */
    public function record_store_has_description()
    {
        // Generate the data
        $attributes = factory('App\Models\Todo')->raw(["description" => ""]);

        // Send
        $response = $this->put(route('todo.store'), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_store_has_max_1000_descr()
    {
        // Generate the data
        $attributes = factory('App\Models\Todo')->raw(["description" => "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop"]);

        // Send
        $response = $this->put(route('todo.store'), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_store_has_status_numeric()
    {
        // Generate the data
        $attributes = factory('App\Models\Todo')->raw(["status" => "Alfa123!@#"]);

        // Send
        $response = $this->put(route('todo.store'), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function user_can_update()
    {
        // Generate the data
        $todo = factory('App\Models\Todo')->create();

        // Send
        $response = $this->patch(route('todo.update', [ "todo" => $todo->todo_id]), $todo->toArray());

        // Assert
        $response->assertStatus(200)->assertSee("success");
    }

    /** @test */
    public function record_update_has_descr()
    {
        // New Record
        $todo = factory('App\Models\Todo')->create();

        // Update fields with invalid data
        $attributes = factory('App\Models\Todo')->raw(["description" => ""]);

        // Send
        $response = $this->patch(route('todo.update', [ "todo" => $todo->todo_id]), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_update_has_max_1000_descr()
    {
        // New Record
        $todo = factory('App\Models\Todo')->create();

        // Update fields with invalid data
        $attributes = factory('App\Models\Todo')->raw(["description" => "qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop"]);

        // Send
        $response = $this->patch(route('todo.update', [ "todo" => $todo->todo_id]), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_update_has_status_numeric()
    {
        // New Record
        $todo = factory('App\Models\Todo')->create();

        // Update fields with invalid
        $attributes = factory('App\Models\Todo')->raw(["status" => "Alfa123!@#"]);

        // Send
        $response = $this->patch(route('todo.update', [ "todo" => $todo->todo_id]), $attributes);

        // Assert
        $response->assertSee("error")->assertStatus(422);
    }

    /** @test */
    public function record_update_status_update()
    {
        // New record
        $todo = factory('App\Models\Todo')->create(["status" => 0]);

        // Send
        $response = $this->patch(route('todo.status', [ "todo" => $todo->todo_id]), ["status" => 1]);

        // Assert warning on inactivate
        $response->assertSee("message")->assertStatus(200);
    }
}
