import React, { Component } from 'react'

class TodosContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todos: [],
      description: '',
      new_todo: '',
      message: ''
    }
    this.createTodo   = this.createTodo.bind(this);
    this.handleStatus   = this.handleStatus.bind(this);
    this.handleNew      = this.handleNew.bind(this);
  }

  getTodos() {
    axios.get('/api/todo')
    .then(response => {
      this.setState({todos: response.data.data,
                     description: ''})
    })
    .catch(error => console.log(error))
  }


  handleNew(event) {
      this.setState({
       new_todo: event.target.value
     })
  }

  handleStatus(event, todo_id, status) {
      event.preventDefault;
      axios.patch('/api/todo/status/'+todo_id, {todo_id: todo_id, status: status})
      .then(response => {
          this.setState({
            message: response.data.message,
            new_todo: ''
          })
          this.getTodos();
      })
      .catch(error => console.log(error))
  }

  createTodo(e) {
    e.preventDefault();
    axios.put('/api/todo/store', {description: this.state.new_todo, status: 0})
    .then(response => {
        this.setState({
          message: response.data.message,
          new_todo: ''
        })
        this.getTodos();
    })
    .catch(error => console.log(error))
 }

  componentDidMount() {
    this.getTodos()
  }


  render() {
    return (
      <div className="row">
          <div className="container-fluid">
              <form onSubmit={this.createTodo} className="row">
                  <div className="input-group mb-3">
                      <input className="form-control"
                             type="text"
                             placeholder="Add a task"
                             maxLength="1000"
                             name="new_todo"
                             value={this.state.new_todo}
                             onChange={this.handleNew}/>
                      <div className="input-group-append">
                          <button className="btn btn-outline-secondary" onClick={this.createTodo} type="button">Add</button>
                      </div>
                  </div>
              </form>
          </div>

          {this.state.message ? (
            <div className="container-fluid">
                <div className="mt-3 col-md-12 alert alert-warning" role="alert">
                    {this.state.message}
                </div>
            </div>) : null }

        	<div className="container mt-3">
        	   <ul className="list-group">
        		  {this.state.todos.map((todo, i) => {
                let Button;
                if (todo.status == 1) {
                   Button = <input type='button' className='btn btn-sm btn-warning' onClick={() => { this.handleStatus(event, todo.todo_id, 0)}} value='Pending' />
                } else {
                   Button = <input type='button' className='btn btn-sm btn-success' onClick={() => { this.handleStatus(event, todo.todo_id, 1)}} value='Done' />
                }

        		    return(
        		      <li className={"list-group-item "+(todo.status==1 ? 'bg-success text-white' : 'bg-warning')} key={i}>
                      <div className="row">
                          <div className="col-md-2 col-sm-2">
                          {todo.status == 1 ? (
                            'Completed'
                          ) : 'Pending'}
                          <br/>
                          {Button}
                          </div>
                          <div className="col-md-10 col-sm-10">
                            {todo.description}
                          </div>
                      </div>
        		      </li>
        		    )
        		  })}
        	   </ul>
        	</div>
      </div>
    )
  }
}

export default TodosContainer
