import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TodosContainer from './TodosContainer'

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="header text-center">
          <h3>Todo List</h3>
        </div>
        <TodosContainer />
      </div>
    );
  }
}

export default App;

if (document.getElementById('index')) {
    ReactDOM.render(<App />, document.getElementById('index'));
}
