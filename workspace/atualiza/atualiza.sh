#!/bin/bash
export PATH=$PATH:/opt/php-7.1/bin:/opt/php-7.1/sbin

USER=1000
GROUP=1000
ENV=Producao
PWD=/var/www/app
GIT=/usr/bin/git
COMPOSER=/usr/local/bin/composer
MODULE=/usr/bin/modulecmd
#PHP=/opt/php-7.1/bin/php

    echo "CRON: Atualizando o ambiente de $ENV"
    cd $PWD

    $GIT fetch origin master-dev
    $GIT reset --hard FETCH_HEAD
    $GIT clean -df

    composer dump-autoload
    php artisan migrate
    php artisan cache:clear
    php artisan view:clear
    php artisan config:clear
    php artisan optimize
    php artisan config:clear    
    chown 1000:1000 /var/www/app/* -R
    php artisan cache:clear
    php artisan config:cache
    php artisan route:cache
    supervisorctl stop all
    supervisorctl start all
  ##
  echo "executar com root 'chown $USER:$GROUP $PWD -R'"
  ##
