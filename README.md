# Requisites
Docker

# Installation
1) Clone repository prepared with Docker and Application
```
git clone git@gitlab.com:ricardo.bruggemann/todo-react.git
```
2) Inside folder up Docker
```
docker-compose up
```

3) Connect workspace
```
docker-compose exec workspace bash
```

4) Download Laravel libraries
```
composer install
```

5) Create database
```
Access phpmyadmin ( http://todo.test:8080/ ) and create database todo ( user root password a )
```

6) Config .env
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:U3NfAMSQuY6p/eemdAgG09226J3Oml4kJwuJJBH3ygo=
APP_DEBUG=true
APP_URL=http://todo.test

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=todo_mysql_1
DB_PORT=3306
DB_DATABASE=todo
DB_USERNAME=root
DB_PASSWORD=a

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
7) Run migrations
```
php artisan migrate
```

8) Add this line to hosts file
```
127.0.0.1 todo.test
```

7) Access http://todo.test
